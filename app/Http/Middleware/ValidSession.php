<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class ValidSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
		$exist = User::whereRaw('last_login < DATE_ADD(NOW(),INTERVAL -1 DAY)')
		->where('id', auth()->user()->id)
		->count();
		if($exist) {
			return redirect('sessions');
		}
		return $next($request);
    }
}
