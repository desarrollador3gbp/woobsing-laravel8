<div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                @if(session()->has('message'))
                <p class="alert alert-info">
                    {{ session()->get('message') }}
                </p>
                @endif
                <form method="POST" action="{{ route('verify.store') }}">
                    {{ csrf_field() }}
                    <h1>Two Factor Verification</h1>
                    <p class="text-muted">
                        You have received an email which contains two factor login code.
                        If you haven't received it, press <a href="{{ route('verify.resend') }}">here</a>.
                    </p>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fa fa-lock"></i>
                            </span>
                        </div>
                        <input name="two_factor_code" type="text" class="form-control" required autofocus placeholder="Two Factor Code">

                    </div>

                    <div class="row">
                        <div class="col-6">
                            <button type="submit" class="btn btn-primary px-4">
                                Verify
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
